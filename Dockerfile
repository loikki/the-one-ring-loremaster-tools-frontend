FROM node:16.16.0
COPY package.json /package.json
COPY package-lock.json /package-lock.json
COPY jsconfig.json /jsconfig.json
COPY babel.config.js /babel.config.js
COPY vue.config.js /vue.config.js
RUN npm install
