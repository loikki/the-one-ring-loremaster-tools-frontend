
# The One Ring - Loremaster Tools Frontend

## How to use it

For the interaction with the backend, please look at the [backend documentation](https://gitlab.com/loikki/the-one-ring-loremaster-tools).

Interactions:
 - If you click a bar (endurance / hope), you can update its value and send this information to the backend.

## Setting up a dev environment

Install nvm: https://github.com/nvm-sh/nvm

`nvm install 16; nvm use 16`


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Images

![alt text](images/player.png)
