import { createApp } from 'vue'
import App from './App.vue'
import { Quasar } from 'quasar'
import quasarUserOptions from './quasar-user-options'

const app = createApp(App)
app.use(Quasar, quasarUserOptions).mount('#app')
app.config.globalProperties.$xp_costs = [4, 8, 12, 20, 26, 30]
